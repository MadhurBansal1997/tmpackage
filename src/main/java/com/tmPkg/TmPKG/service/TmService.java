package com.tmPkg.TmPKG.service;

public interface TmService {
  Integer getCouponValidity(String pStr, Integer pCnt);
  Integer getCouponUsageCount(String pStr, Integer pCnt);
}
