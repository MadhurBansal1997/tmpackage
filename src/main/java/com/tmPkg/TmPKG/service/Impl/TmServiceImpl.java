package com.tmPkg.TmPKG.service.Impl;
import com.tmPkg.TmPKG.repository.CountRepository;
import com.tmPkg.TmPKG.service.TmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TmServiceImpl implements TmService {

  @Autowired
  CountRepository countRepository;

  private Integer getSingleCouponUsageCount(String couponCode) {
    Integer singleCouponUsageCount;
    singleCouponUsageCount = countRepository.findSingleCouponUsageCount(couponCode);
    return singleCouponUsageCount;
  }

  public Integer getCouponValidity(String pStr, Integer pCnt) {
    int vCnt;
    String[] tokenList;
    try {
      tokenList = pStr.split(",");
      for(String i:tokenList) {
        vCnt = getSingleCouponUsageCount(i);
        if(vCnt >= pCnt)
          return 0;
      }
      return 1;
    } catch (Exception e) {
      log.info("getCouponValidity Failed!!");
    }
    return null;
  }

  public Integer getCouponUsageCount(String pStr, Integer pCnt) {
    int vCnt=0;
    String[] tokenList;
    try {
      tokenList = pStr.split(",");
      for(String i:tokenList) {
        vCnt = getSingleCouponUsageCount(i);
        if(vCnt >= pCnt)
          return vCnt;
      }
      return vCnt;
    } catch (Exception e) {
      log.info("getCouponUsageCount Failed!!");
    }
    return null;
  }
}