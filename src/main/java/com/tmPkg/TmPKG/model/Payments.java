package com.tmPkg.TmPKG.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(
        name = "payments",
        schema = "tm",
        catalog = "tm"
)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payments implements Serializable {
  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "trip_id")
  private Long tripId;

  @Column(name = "seq_no")
  private Long sequenceNumber;

  @Column(name = "status")
  private char status;
}
