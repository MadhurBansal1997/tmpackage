package com.tmPkg.TmPKG.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
        name = "trips",
        schema = "tm",
        catalog = "tm"
)
public class Trips implements Serializable {
  @Id
  @Column(name = "id")
  long id;
}
