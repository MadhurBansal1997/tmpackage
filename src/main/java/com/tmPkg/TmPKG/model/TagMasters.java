package com.tmPkg.TmPKG.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
        name = "tag_masters",
        schema = "tm",
        catalog = "tm"
)
public class TagMasters{
  @Id
  @Column(name = "id")
  Long id;

  @Column(name = "tag_name")
  String tagName;

}