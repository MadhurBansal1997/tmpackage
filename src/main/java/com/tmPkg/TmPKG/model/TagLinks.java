package com.tmPkg.TmPKG.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(
        name = "tag_links",
        schema = "tm",
        catalog = "tm"
)
@Data
public class TagLinks {

  @Id
  @Column(name = "id")
  Long id;

  @Column(name = "tag_id")
  Long tagId;

  @Column(name = "tag_type")
  String tagType;

  @Column(name = "tag_master_id")
  Long tagMasterId;

}
