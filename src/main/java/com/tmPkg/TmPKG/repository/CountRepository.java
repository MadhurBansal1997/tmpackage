package com.tmPkg.TmPKG.repository;

import com.tmPkg.TmPKG.model.TagMasters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface CountRepository extends JpaRepository<TagMasters, Long> {
  @Query(value =
          "select count(*) FROM com.tmPkg.TmPKG.model.TagMasters tm join com.tmPkg.TmPKG.model.TagLinks tl on tm.id = tl.tagMasterId" +
                  " join com.tmPkg.TmPKG.model.Trips t on tl.tagId = t.id join com.tmPkg.TmPKG.model.Payments p on t.id = p.tripId " +
                  " where tm.tagName = concat('Coupon:',:couponCode)" +
                  " and tl.tagType = 'Trip' and p.sequenceNumber = 1 and p.status = 'S'"
  )
  Integer findSingleCouponUsageCount(@Param("couponCode") String couponCode);
}
