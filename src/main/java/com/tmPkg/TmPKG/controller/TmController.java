package com.tmPkg.TmPKG.controller;

import com.tmPkg.TmPKG.service.TmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commonservice")
@Slf4j
public class TmController {
  @Autowired
  TmService tmService;

  @GetMapping("/coupon-usage-count-validate")
  public String singleCouponController(@RequestParam(name = "coupon-codes") String couponCode, @RequestParam(name = "count") Integer count,
                                       @RequestParam(name = "pprv", required = false) String pprv) {
    Integer res;
    if("Y".equals(pprv)) {
      res = tmService.getCouponUsageCount(couponCode, count);
      return (res != null) ? res.toString() : null;
    }
    else if(pprv == null) {
      res = tmService.getCouponValidity(couponCode, count);
      return (res != null) ? res.toString() : null;
    }
    else
      return null;
  }
}
