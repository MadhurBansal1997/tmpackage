package com.tmPkg.TmPKG;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmPkgApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmPkgApplication.class, args);
	}
}